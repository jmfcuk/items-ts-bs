import * as React from 'react';
import { Row, Col } from 'react-bootstrap';
import ItemDef from './Item.d';

interface ItemReadOnlyProps {
    item: ItemDef;
    onSelectClick: () => void;
    onDeleteClick: () => void;
}

const ItemReadOnly: React.SFC<ItemReadOnlyProps> = (props: ItemReadOnlyProps): JSX.Element => {

    return (
        <div className="text-center">
            <Row>
                <Col xs={3}>
                    <Row>
                        <span>
                            <div className="glyphicon glyphicon-pencil"
                                onClick={props.onSelectClick} />
                            <div className="glyphicon glyphicon-trash"
                                onClick={props.onDeleteClick} />
                        </span>
                    </Row>
                    <Row>
                        <div>
                            <input type="text" id="name" className="disabled-input text-center"
                                defaultValue={props.item.name} />
                        </div>
                        <div>
                            <input type="text" id="description" className="disabled-input text-center"
                                defaultValue={props.item.description} />
                        </div>
                        <div><a href={props.item.itemUri}>{props.item.itemUri}</a></div>
                    </Row>
                </Col>
                <Col xs={9}>
                    <img className="img-responsive" src={props.item.imageUri} />
                </Col>
            </Row>
        </div>
    );
};

export default ItemReadOnly;
