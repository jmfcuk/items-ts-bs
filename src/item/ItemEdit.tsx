import * as React from 'react';
import { Row, Col } from 'react-bootstrap';
import ItemDef from './Item.d';

interface ItemEditProps {
    item: ItemDef;
    onSave: (item: ItemDef) => void;
    onCancel: () => void;
}

interface ItemEditState {
    item: ItemDef;
}

class ItemEdit extends React.Component<ItemEditProps, ItemEditState> {

    state: ItemEditState;

    constructor(props: ItemEditProps) {
        super(props);

        this.state = {
            item: {
                id: props.item.id,
                category: props.item.category,
                name: props.item.name,
                description: props.item.description,
                data: props.item,
                itemUri: props.item.itemUri,
                imageUri: props.item.imageUri
            },
        };
    }

    save = () => {
        this.props.onSave(this.state.item);
    }

    cancel = () => {
        this.props.onCancel();
    }

    nameChange = (e: any) => {

        let item = this.state.item;

        item.name = e.target.value;

        this.setState({ item: item });
    }

    descriptionChange = (e: any) => {

        let item = this.state.item;

        item.description = e.target.value;

        this.setState({ item: item });
    }

    render(): JSX.Element {

        return (
            <div className="border text-center">
                <Row>
                    <Col xs={3}>
                        <span>
                            <div className="glyphicon glyphicon-thumbs-up"
                                onClick={this.save} />
                            <div className="glyphicon glyphicon-thumbs-down"
                                onClick={this.cancel} />
                        </span>
                        <div>
                            <input type="text" id="name" className="text-center"
                                defaultValue={this.props.item.name}
                                onChange={this.nameChange} />
                        </div>
                        <div>
                            <input type="text" id="description" className="text-center"
                                defaultValue={this.state.item.description}
                                onChange={this.descriptionChange} />
                        </div>
                        <div><a href={this.props.item.itemUri}>{this.props.item.itemUri}</a></div>
                    </Col>
                    <Col xs={9}>
                        <img className="img-responsive" src={this.state.item.imageUri} />
                    </Col>
                </Row>
            </div>
        );
    }
}

export default ItemEdit;
