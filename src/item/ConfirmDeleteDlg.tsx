import * as React from 'react';
import { Modal, Button } from 'react-bootstrap';

interface ConfirmDeleteDlgProps {
    name: string;
    open: boolean;
    onResult: (confirm: boolean) => void;
}

const ConfirmDeleteDlg: React.SFC<ConfirmDeleteDlgProps> = (props: ConfirmDeleteDlgProps): JSX.Element => {

    return(
        <Modal className="text-center" onHide={() => {}} show={props.open}>
            <div >Delete {props.name}?</div>
            <div>
                <Button onClick={() => props.onResult(true)}>Yes</Button>
                <Button onClick={() => props.onResult(false)}>No</Button>
            </div>
        </Modal>
    );
};

export default ConfirmDeleteDlg;
