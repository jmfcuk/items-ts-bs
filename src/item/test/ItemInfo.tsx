import * as React from 'react';
import ItemDef from '../Item.d';

interface ItemInfoProps {
    item: ItemDef;
    onEditClick: () => void;
    onDeleteClick: () => void;
}

const ItemInfo: React.SFC<ItemInfoProps> = (props: ItemInfoProps): JSX.Element => {

    return (
        <div className="border text-center">
            <span>
                <div className="glyphicon glyphicon-pencil"
                    onClick={props.onEditClick} />
                <div className="glyphicon glyphicon-trash"
                    onClick={props.onDeleteClick} />
            </span>
            <div>
                <input type="text" id="name" className="disabled-input text-center"
                    defaultValue={props.item.name} />
            </div>
            <div>
                <input type="text" id="description" className="disabled-input text-center"
                    defaultValue={props.item.description} />
            </div>
            <div><a href={props.item.itemUri}>{props.item.itemUri}</a></div>
        </div>
    )
}

export default ItemInfo;
