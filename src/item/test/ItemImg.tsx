import * as React from 'react';

interface ItemImgProps {
    uri: string;
}

const ItemImg = (props: ItemImgProps): JSX.Element => {

    return (
        <img className="img-responsive" src={props.uri} />
    )
}

export default ItemImg;
