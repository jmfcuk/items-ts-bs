import * as React from 'react';
import ItemDef from './Item.d';
import {
    Grid,
    Row,
    Col,
    Button
} from 'react-bootstrap';
import ItemBrowser from './ItemBrowser';

interface ItemListState {
    items: Array<ItemDef>;
}

class ItemList extends React.Component<any, ItemListState> {

    state: ItemListState;

    constructor(props: any) {

        super(props);

        this.state = {
            items: []
        };
    }

    componentDidMount() {
        this.refreshList();
    }

    getList = (reset: boolean = false) => {

        let url = 'http://127.0.0.1:5000/';

        if (reset) {
            url += 'reset';
        } else {
            url += 'items';
        }

        fetch(url)
            .then(response => response.json())
            .then(data => this.setState({ items: data.items }));
    }

    refreshList = () => {
        this.getList();
    }

    resetList = () => {
        this.getList(true);
    }

    delete = (item: ItemDef) => {

        const init: any = {
            method: 'DELETE',
            mode: 'cors',
            cache: 'default'
        };

        fetch(item.itemUri, init)
            .then(response => response.json())
            .then(data => {
                const filteredItems = this.state.items.filter(it => {
                    return it.id !== item.id;
                });

                this.setState({ items: filteredItems });
            });
    }

    save = (item: ItemDef) => {

        let hdrs = new Headers();

        hdrs.append('Content-Type', 'application/json');

        const init: any = {
            method: 'PUT',
            mode: 'cors',
            cache: 'default',
            body: JSON.stringify(item),
            headers: hdrs
        };

        fetch(item.itemUri, init)
            .then(response => response.json())
            .then(data => {
                let its: Array<ItemDef> = this.state.items;

                const idx = its.findIndex(it => item.id === it.id);

                if(-1 < idx)
                    its[idx] = item;

                this.setState({items: its});
            });
    }

    render(): JSX.Element {

        return (
            <div>
                <Grid>
                    <Row className="text-center">
                        <Button onClick={this.refreshList}>Refresh</Button>
                        <Button onClick={this.resetList}>Reset</Button>
                    </Row>
                    <Row>
                        <Col xs={12}>
                            <ItemBrowser ibid="ibid1" 
                                         items={this.state.items} 
                                         onDelete={this.delete}
                                         onSave={this.save}/>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }

    getItemList = (): string => {

        let s = '';

        for(var item of this.state.items) {
            s+= item.name + ', ';
        }

        return s;
    }
}

export default ItemList;
