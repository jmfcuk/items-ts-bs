import * as React from 'react';
import ItemDef from './Item.d';
import Item from './Item';
import {
    Row,
    Carousel,
    CarouselItem,
    DropdownButton,
    MenuItem
} from 'react-bootstrap';

interface ItemBrowserProps {
    ibid: string;
    items: Array<ItemDef>;
    onDelete: (item: ItemDef) => void;
    onSave: (item: ItemDef) => void;
}

interface ItemBrowserState {
    filter: string;
}

class ItemBrowser extends React.Component<ItemBrowserProps, ItemBrowserState> {

    catAll: string = 'All';
    cat1: string = 'Cat1';
    cat2: string = 'Cat2';
    cat3: string = 'Cat3';

    state: ItemBrowserState;

    constructor(props: ItemBrowserProps) {

        super(props);

        this.state = {
            filter: this.catAll
        };
    }

    save = (item: ItemDef) => {
        this.props.onSave(item);
    }

    delete = (item: ItemDef) => {
        this.props.onDelete(item);
    }

    render(): JSX.Element {
        return (
            <div>
                <Row>
                    <span>
                        <DropdownButton id={this.props.ibid} title={this.state.filter}>
                            <MenuItem onSelect={() => this.setState({ filter: this.catAll })} >{this.catAll}</MenuItem>
                            <MenuItem onSelect={() => this.setState({ filter: this.cat1 })} >{this.cat1}</MenuItem>
                            <MenuItem onSelect={() => this.setState({ filter: this.cat2 })} >{this.cat2}</MenuItem>
                            <MenuItem onSelect={() => this.setState({ filter: this.cat3 })} >{this.cat3}</MenuItem>
                        </DropdownButton>
                        <div className="pull-right">
                            Category: {this.state.filter}
                        </div>
                    </span>
                </Row>
                <Row>
                    <Carousel slide={true}
                        controls={true}
                        indicators={false}
                        interval={0}
                        className="text-center">
                        {this.renderList()}
                    </Carousel>
                </Row>
            </div>
        );
    }

    renderList = () => {

        let filter = this.state.filter;

        if (filter === this.catAll) {
            return this.renderAll();
        }

        const filteredItems = this.props.items.filter((it: ItemDef) => {
            return it.category.toUpperCase() === filter.toUpperCase();
        });

        return filteredItems.map((item: ItemDef) => (
            <CarouselItem key={item.id}>
                <Item
                    key={item.id}
                    item={item}
                    onDelete={this.delete}
                    onSave={this.save}
                />
            </CarouselItem>
        ));
    }

    renderAll = () => {
        return this.props.items.map((item: ItemDef) => (
            <CarouselItem key={item.id}>
                <div>
                <Item
                    key={item.id}
                    item={item}
                    onDelete={this.delete}
                    onSave={this.save}
                />
                </div>
            </CarouselItem>
        ));
    }
}

export default ItemBrowser;

