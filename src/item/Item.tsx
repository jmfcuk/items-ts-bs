import * as React from 'react';
import ItemDef from './Item.d';
import ItemReadOnly from './ItemReadOnly';
import ItemEdit from './ItemEdit';
import ConfirmDeleteDlg from './ConfirmDeleteDlg';
import './Item.css';

interface ItemProps {
  item: ItemDef;
  onDelete: (item: ItemDef) => void;
  onSave: (item: ItemDef) => void;
}

interface ItemState {
  item: ItemDef;
  editMode: boolean;
  warnDelete: boolean;
}

class Item extends React.Component<ItemProps, ItemState> {

  state: ItemState;

  constructor(props: ItemProps) {

    super(props);

    this.state = {
      item: {
        id: props.item.id,
        category: props.item.category,
        name: props.item.name,
        description: props.item.description,
        data: props.item.data,
        itemUri: props.item.itemUri,
        imageUri: props.item.imageUri
      },
      editMode: false,
      warnDelete: false
    };
  }

  select = () => {
    this.setState({ editMode: true });
  }

  editSave = (item: ItemDef) => {

    this.setState({ editMode: false, item: item });

    this.props.onSave(item);
  }

  editCancel = () => {
    this.setState({ editMode: false });
  }

  delete = () => {
    this.setState({ warnDelete: true });
  }

  deleteConfirmed = (confirm: boolean) => {
    if (confirm) {
      this.props.onDelete(this.state.item);
    }
    this.setState({ warnDelete: false });
  }

  render(): JSX.Element {

    let jsx = null;

    if (!this.state.editMode) {
      jsx =
        (
          <div>
            <ItemReadOnly item={this.state.item}
              onSelectClick={this.select}
              onDeleteClick={this.delete} />
              <ConfirmDeleteDlg name={this.props.item.name} 
                                open={this.state.warnDelete}
                                onResult={this.deleteConfirmed} />
            </div>
        );
    } else {
      jsx =
        (
          <ItemEdit item={this.props.item}
                    onSave={this.editSave}
                    onCancel={this.editCancel} />
        );
    }

    return jsx;
  }
}

export default Item;
