
export interface ItemHeaderDef {
  id: number;
  category: string;
  name: string;
  description: string;
  itemUri: string;
  imageUri: string;
}

export interface ItemDef extends ItemHeaderDef {
    data: object;
}

export default ItemDef;
