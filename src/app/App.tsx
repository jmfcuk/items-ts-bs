import * as React from 'react';
import ItemList from '../item/ItemList';
import { Grid, Row } from 'react-bootstrap';

const App = () => {
  return (
    <Grid>
      <Row>
        <ItemList />
      </Row>
      <Row>
      </Row>
    </Grid>
  );
};

export default App;
